from ST7735 import TFT,TFTColor
from machine import SPI,Pin
import utime

spi = SPI(2, baudrate=20000000, polarity=0, phase=0, sck=Pin(18), mosi=Pin(17), miso=Pin(19))
tft = TFT(spi,14,15,16)
tft.initr()
tft.rgb(True)
tft.fill(TFT.BLACK)

def triangle(pt1,pt2,pt3):
    '''Draw a white triangle from the 3 points.
    ptX is [x,y]'''
    tft.line(pt1,pt2,TFT.WHITE)
    tft.line(pt2,pt3,TFT.WHITE)
    tft.line(pt1,pt3,TFT.WHITE)

tft.fill(TFT.BLACK)
for pos in range(128):
    tft.fill(TFT.BLACK)
    triangle([pos,10],[pos+2,20],[pos,20])
    
print("done")
