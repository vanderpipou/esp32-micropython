import displayctl

class Menu(object):
    CURSOR = '>'

    def __init__(self, dispctl, pos=(0,0), name="Menu"):
        self.display = dispctl
        self.name = name
        self.cursor_pos = 0
        self.obj_list = []
        self.action_list = []
        self.position = pos
        

    def add_selection(self, txt, action):
        self.obj_list.append(txt)
        self.action_list.append(action)
        return len(self.obj_list)-1
    
    def cursor_move_up(self):
        if self.cursor_pos == 0:
            self.cursor_pos = len(self.obj_list)-1
        else:
            self.cursor_pos = self.cursor_pos-1
        self.display.render()
    
    def cursor_move_down(self):
        if self.cursor_pos == len(self.obj_list)-1:
            self.cursor_pos = 0
        else:
            self.cursor_pos = self.cursor_pos+1
        self.display.render()

    def select(self):
        try:
            self.action_list[self.cursor_pos]()
        except:
            print("Function invalid")
        return self.obj_list[self.cursor_pos]

    def draw(self):
        if len(self.obj_list) == 0:
            return
        #Display menu name
        self.display.disp.text(self.position,self.name,self.display.disp.WHITE,self.display.sysfont)
        #Display menu objects
        for x in range(len(self.obj_list)):
            postxt = (self.position[0]+6,self.position[1]+x*8+8)
            self.display.disp.text(postxt,self.obj_list[x],self.display.disp.WHITE,self.display.sysfont)
        #Display cursor
        cursorpos = (self.position[0],self.position[1]+self.cursor_pos*8+8)
        self.display.disp.char(cursorpos,self.CURSOR, self.display.disp.WHITE, self.display.sysfont, (1,1))




