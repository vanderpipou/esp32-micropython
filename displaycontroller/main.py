import network
from ST7735 import TFT
from machine import SPI,Pin
import utime
import sysfont
import _thread
from displayctl import Dispctl
from menu import Menu
from buttonmngr import ButtonMngr, defaultfoo

spi = SPI(2, baudrate=20000000, polarity=0, phase=0, sck=Pin(18), mosi=Pin(17), miso=Pin(19))
tft = TFT(spi,14,15,16)
tft.rotation(1)
tft.initr()
tft.rgb(True)
tft.fill(TFT.BLACK)

dispctl = Dispctl(tft, sysfont.sysfont)
menu = Menu(dispctl)
btn = ButtonMngr(21, 25, 23, 22)

def doconnect():
  sta_if = network.WLAN(network.STA_IF)
  if not sta_if.isconnected() :
    print('Connecting to dyos')
    sta_if.active(True)
    sta_if.connect('dyos', 'ethzpadzo')
    while not sta_if.isconnected():
      pass
  print('network ip: ', sta_if.ifconfig()[0])

def run():
#    _thread.start_new_thread(mainthread,("main",10))
#    print("Done")
#    dispctl = Dispctl(tft, sysfont.sysfont)
#    menu = Menu(dispctl)
  menu.add_selection("Hello",foo1)
  menu.add_selection("Help",foo2)
  btn.set_actions(menu.cursor_move_up, menu.cursor_move_down, menu.select, foo)
  dispctl.add_to_render(menu)
  dispctl.render()
  

def foo():
  print("foo...")
def foo1():
  print("foo1...")
def foo2():
  print("foo2...")


def mainthread(name ,xrange):
  for a in range(xrange):
    msg = name+" thread: "+str(a)
    tft.text((0,a*sysfont.sysfont.get("Height")),msg,TFT.WHITE,sysfont.sysfont)
  
  return 0

