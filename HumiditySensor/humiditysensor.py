from machine import Pin, ADC
from time import sleep

humSensor = ADC(Pin(34))
humSensor.atten(ADC.ATTN_11DB) # Attenuation to read 0V to 3.6V
humSensor.width(ADC.WIDTH_12BIT) # output 12bit (4096)

while True:
    print("Humidity=",humSensor.read())
    sleep(1)