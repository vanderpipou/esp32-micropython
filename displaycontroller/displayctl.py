

class Dispctl(object):
    '''Display Controller: Build frame to render.
    Every object that will be in the rendre pipeline must have a draw() methode'''
    def __init__(self, display, sysfont):
        self.disp = display
        self.sysfont = sysfont
        self.pipe = []
    
    def add_to_render(self, render_object):
        self.pipe.append(render_object)

    def render(self):
        #self.disp._reset()
        self.disp.fill()
        for render_object in self.pipe:
            try:
                render_object.draw()
            except AttributeError:
                print("Object "+str(render_object)+" couldn\'t be drawn!")