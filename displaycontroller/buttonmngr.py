from machine import Pin

class ButtonMngr(object):
    def __init__(self, pinu, pind, pinr, pinl):
        # set up pin
        self.up = Pin(pinu, Pin.IN, Pin.PULL_UP)
        self.down = Pin(pind, Pin.IN, Pin.PULL_UP)
        self.right = Pin(pinr, Pin.IN, Pin.PULL_UP)
        self.left = Pin(pinl, Pin.IN, Pin.PULL_UP)
        # set default action
        self.actup = defaultfoo
        self.actdo = defaultfoo
        self.actri = defaultfoo
        self.actle = defaultfoo

    def set_up_action(self,action):
        self.actup = action
        self.up.irq(trigger=Pin.IRQ_FALLING, handler=self.__action_up)

    def set_down_action(self,action):
        self.actdo = action
        self.down.irq(trigger=Pin.IRQ_FALLING, handler=self.__action_down)

    def set_right_action(self,action):
        self.actri = action
        self.right.irq(trigger=Pin.IRQ_FALLING, handler=self.__action_right)

    def set_left_action(self,action):
        self.actle = action
        self.left.irq(trigger=Pin.IRQ_FALLING, handler=self.__action_left)
    
    def set_actions(self, actu, actd, actr, actl):
        self.set_up_action(actu)
        self.set_down_action(actd)
        self.set_right_action(actr)
        self.set_left_action(actl)

    def __action_up(self, pin):
        print("up")
        try:
            self.actup()
        except:
            pass
    def __action_down(self, pin):
        print("down")
        try:
            self.actdo()
        except:
            pass
    def __action_right(self, pin):
        print("right")
        try:
            self.actri()
        except:
            pass
    def __action_left(self, pin):
        print("left")
        try:
            self.actle()
        except:
            pass

def defaultfoo(pin):
    print("default on pin ",pin)
